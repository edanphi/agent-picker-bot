import discord
import os
from discord.ext import commands
from dotenv import load_dotenv
import random
import pymongo
from discord.raw_models import RawReactionActionEvent
import datetime
from functools import partial
import math

load_dotenv()

TOKEN = os.getenv('BOT_TOKEN')
DB_URL = os.getenv('DB_URL')
FROM_NOT_SELECTED_BUTTON_LABEL = os.getenv('FROM_NOT_SELECTED_BUTTON_LABEL')
FROM_SELECTED_BUTTON_LABEL = os.getenv('FROM_SELECTED_BUTTON_LABEL')
FROM_ALL_BUTTON_LABEL = os.getenv('FROM_ALL_BUTTON_LABEL')
DEFAULT_CHANNEL_NAME = os.getenv('DEFAULT_CHANNEL_NAME')
MAX_REACTIONS_PER_ROW = os.getenv('MAX_REACTIONS_PER_ROW')

intents = discord.Intents.default()
intents.reactions = True
intents.messages = True
bot = commands.Bot(command_prefix="!", intents=intents)

client = pymongo.MongoClient(DB_URL)
db = client['agent_picker_bot_db']
reactions_collection = db['user_channel_reactions']
channels_collection = db['channels']

# Constant embed content
embed_content = {
    "title": "VALORANT RANDOM AGENT PICKER",
    "description": "Use the reactions to select agents.\nClick the buttons bellow to pick a random agent.\nRoll from:",
    "color": 0xFF4756,
}   

# Reaction emojis
reaction_emojis = [
    "<:astra:1287149645704859688>",
    "<:breach:1287149648275836979>",
    "<:brimstone:1287149650696081521>",
    "<:chamber:1287149653061537792>",
    "<:clove:1287149654567420066>",
    "<:cypher:1287149656282759239>",
    "<:deadlock:1287149657637654590>",
    "<:fade:1287149660200239144>",
    "<:gekko:1287149662343659601>",
    "<:harbor:1287149663924785153>",
    "<:iso:1287149666022199398>",
    "<:jett:1287149668387524638>",
    "<:kayo:1287149960386707627>",
    "<:killjoy:1287149672103678064>",
    "<:neon:1287149673710227596>",
    "<:omen:1287149981379330181>",
    "<:phoenix:1287149678261178378>",
    "<:raze:1287149681176215663>",
    "<:reyna:1287149995556081795>",
    "<:sage:1287149685185974370>",
    "<:skye:1287149687874256976>",
    "<:sova:1287150022932037692>",
    "<:viper:1287149692320223232>",
    "<:vyse:1287150055236698142>",
    "<:yoru:1287149695810011187>",
    "<:tejo:1328082274926989312>"
]

class roll_buttons(discord.ui.View):
    def __init__(self):
        super().__init__(timeout=None)  
        roll_from_not_selected_button = discord.ui.Button(emoji="⬜", label=FROM_NOT_SELECTED_BUTTON_LABEL, style=discord.ButtonStyle.red, custom_id="roll_from_not_selected_button")
        roll_from_selected_button = discord.ui.Button(emoji="⏹️", label=FROM_SELECTED_BUTTON_LABEL, style=discord.ButtonStyle.red, custom_id="roll_from_selected_button")
        roll_from_all_button = discord.ui.Button(label=FROM_ALL_BUTTON_LABEL, style=discord.ButtonStyle.red, custom_id="roll_from_all_button")
        
        roll_from_not_selected_button.callback = partial(self.button_callback, roll_from="not selected")
        roll_from_selected_button.callback = partial(self.button_callback, roll_from="selected")
        roll_from_all_button.callback = partial(self.button_callback, roll_from="all")

        self.add_item(roll_from_not_selected_button)
        self.add_item(roll_from_selected_button)
        self.add_item(roll_from_all_button)


    async def button_callback(self, interaction: discord.Interaction, roll_from: str):
        user_doc = reactions_collection.find_one({'user_id': interaction.user.id, 'channel_id': interaction.channel_id})
        selected_emojis = [] if not user_doc else user_doc['reactions']
        
        emoji_roll_pool = []

        match roll_from:
            case "not selected":
                emoji_roll_pool = [emoji for emoji in reaction_emojis if emoji not in selected_emojis]
            case "selected":
                emoji_roll_pool = selected_emojis
            case "all":
                emoji_roll_pool = reaction_emojis
        
        if (len(emoji_roll_pool) == 0):
            await interaction.response.send_message("Cannot select from an empty set.", ephemeral=True)
            return
        
        random_emoji = random.choice(emoji_roll_pool)

        await interaction.response.send_message(random_emoji, ephemeral=True)
        print(f"{datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")}: The user: {interaction.user.name} has used the random agent picker and got: {random_emoji}")


@bot.tree.command()
async def create_agent_picker(interaction: discord.Interaction, channel_id: str = None):
    try:
        channel = None
        if channel_id:
            if channels_collection.find_one({'channel_id': channel_id}):
                await interaction.response.send_message("Cannot create the embed inside a channel with another agent picker in it", ephemeral=True)    
                return

            channel = interaction.guild.get_channel(int(channel_id))
            await interaction.response.send_message("Creating embed", ephemeral=True)
        else:
            overwrites = {
                interaction.guild.default_role: discord.PermissionOverwrite(send_messages=False, add_reactions=False, read_messages=True),
            }
            await interaction.response.send_message("creating channel", ephemeral=True)
            channel = await interaction.guild.create_text_channel(DEFAULT_CHANNEL_NAME, overwrites=overwrites)
        
        channels_collection.insert_one({'channel_id': channel.id})

        if channel is None:
            await interaction.response.send_message("Invalid channel ID.", ephemeral=True)
            return

        message_amount = math.ceil(len(reaction_emojis) / int(MAX_REACTIONS_PER_ROW))
        embed_message = await channel.send(embed=discord.Embed.from_dict(embed_content), view=roll_buttons())
        reaction_messages = [(await channel.send(content="‎", suppress_embeds=True)) for i in range(message_amount - 1)]
        reaction_messages.insert(0, embed_message)

        # Add reaction emojis
        for index, message in enumerate(reaction_messages):
            for emoji in reaction_emojis[index * int(MAX_REACTIONS_PER_ROW) : min((index + 1) * int(MAX_REACTIONS_PER_ROW), len(reaction_emojis))]:
                await message.add_reaction(emoji)
    
    except discord.HTTPException as e:
        await interaction.response.send_message(f"Error creating embed: {e}", ephemeral=True)


@bot.event
async def on_raw_reaction_add(payload: RawReactionActionEvent):
    channel_id = payload.channel_id
    channel_doc = channels_collection.find_one({'channel_id': channel_id})

    if bot.user.id == payload.user_id or not channel_doc:
        return

    user_id = payload.user_id
    emoji_to_add = str(payload.emoji)
    user_doc = reactions_collection.find_one({'user_id': user_id, 'channel_id': channel_id})

    if user_doc:
        reactions_collection.update_one({'user_id': user_id, 'channel_id': channel_id}, {'$addToSet': {'reactions': emoji_to_add}})
    else:
        user_doc = {'user_id': user_id, 'channel_id': channel_id, 'reactions': [emoji_to_add]}
        reactions_collection.insert_one(user_doc)


@bot.event
async def on_raw_reaction_remove(payload: RawReactionActionEvent):
    channel_id = payload.channel_id
    channel_doc = channels_collection.find_one({'channel_id': channel_id})

    if bot.user.id == payload.user_id or not channel_doc:
        return
    
    user_id = payload.user_id
    emoji_to_remove = str(payload.emoji)
    user_doc = reactions_collection.find_one({'user_id': user_id, 'channel_id': channel_id})
    
    if user_doc:
        reactions_collection.update_one({'user_id': user_id}, {'$pull': {'reactions': emoji_to_remove}})
        if len(reactions_collection.find_one({'user_id': user_id, 'channel_id': channel_id})['reactions']) == 0:
            reactions_collection.delete_one({'user_id': user_id, 'channel_id': channel_id})


@bot.event
async def on_guild_channel_delete(channel: discord.abc.GuildChannel):
    channels_collection.delete_one({'channel_id': channel.id})
    reactions_collection.delete_many({'channel_id': channel.id})


@bot.event
async def on_ready():
    print(f'We have logged in as {bot.user.name}')
    try:
        synced = await bot.tree.sync()
        print(f"Synced {len(synced)} command(s)")
    except Exception as e:
        print(e)

async def setup_hook():
    bot.add_view(roll_buttons())

bot.setup_hook = setup_hook

bot.run(TOKEN)